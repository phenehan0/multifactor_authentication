import pyotp

def otp(secret, interval=30):
    password = pyotp.TOTP(secret, interval=interval)
    print(password.now())
    return password.now()
