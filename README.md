MFA with Python, ScenarioBuilder, and Authenticator App
=============================================================

I.) Using Python to generate OTP
-----------------------------------------------
__Install the pyotp library__


```
pip install pyotp
```

__Using the pyotp library in OTP.py allows you to generate, a six-digit one time passcode from a security key.__


```
import pyotp

def otp(secret, interval=30):
	passcode = pyotp.TOTP(secret, interval=interval)
	print(password.now())

#example
security_key = “fhsnvb56w2mgyeitm24”
otp(security_key) #returns 6-digit auth code

```

II.) Setting up 2FA in SalesForce.
-----------------------------------------------


__Navigate to your personal settings on  SalesForce and select ‘advanced user details’ from the menu.__


![fig.1](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/Fig.1.png)


__In this section, you will be provided with the option to register a one-time password authenticator from a third party app (e.g. Google authenticator).__


![fig.2](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/fig.2.png)


__If you select this option, SalesForce will once again prompt you for your login credentials. Once you’ve successfully logged in, you should see the following screen--be sure to select the “I can’t scan the QR code” option on the bottom right.__


![fig.3](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/fig.3.png)


__You will then be given a code which you can enter into Google authenticator. Make sure to save this secret key somewhere so that Python and ScenarioBuilder can access it.__


![fig.4](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/fig.4.png)


III.) Using ScenarioBuilder
----------------------------

Defining variables:
-------------------


__Create a CSV file that looks something like this:__


![example_csv](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/csv_ex.png)



__Create a Process Scenario and define *.csv) as its variable file:__


![csv_var](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/define_csv_var.png)


![csv_var](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/process_Sc1.png)



Extracting your one-time passcode from the Python script:
---------------------------------------------------------


__To do this, you can use ScenarioBuilder’s “run command line” action. In the ‘program/command’ section, call the TOTP function with %securitykey% passed as its ‘securitykey’ argument.__



![cmd1](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/cmd1.png)



![cmd2](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/cmd2.png)


__Now, if you define the output of the function call as a variable...__



![strvar](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/strvar.png)



![strvar2](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/strvar2.png)


__Here, the bot will navigate to “login.salesforce.com”. Then, using its image recognition and keyboard actions, the bot will:__


__I.)  Navigate to the username form field__

__II.) Enter the %username% variable (taken from the CSV) into the username form field__

__III.) substitute ‘password’ for ‘username’ and repeat I.) and II.)__

__IV.) substitute ‘passcode’ for ‘username’ and repeat I.) and II.)__



![step1](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/step1.png)


![step2](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/step2.png)


![step3](https://bitbucket.org/phenehan0/multifactor_authentication/downloads/step3.png)